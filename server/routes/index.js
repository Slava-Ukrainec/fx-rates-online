const express = require('express');
const router = express.Router();
const fetch = require("node-fetch");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/currencies', function(req, res, next) {
  fetch('http://resources.finance.ua/ua/public/currency-cash.json')
      .then(response=>response.text(),reject=>new Error('Unable to get data from finance.ua')).then(text=>res.send(text));
});

module.exports = router;
