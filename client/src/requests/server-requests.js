import serverUrl from './config'

function testConnection() {
    fetch(`${serverUrl}/testAPI`)
        .then(res => res.text())
        .then(res => this.setState({serverResponse: res}));
}


function getCurrenciesAPI() {
     return fetch(`${serverUrl}/currencies`)
        .then(res => res.json())
        // .then(res => JSON.parse(res))
}

const serverRequests =  {
        testConnection,
        getCurrenciesAPI
    };

export default serverRequests