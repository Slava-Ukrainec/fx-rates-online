import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Assignment from "@material-ui/icons/Assignment"
import CustomInput from "components/CustomInput/CustomInput.jsx";
import InputAdornment from "@material-ui/core/InputAdornment";

// core components
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";

class CustomTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData,
            filteredColumns: [],
            sortedColumn: ''
        }
    };

    sortColumn = (evt, key) => {
        const {tableData} = this.state;
        if (this.state.sortedColumn !== key) {
            tableData.sort((a, b) => {
                return isNaN(+a[key]) ? a[key].localeCompare(b[key]) : a[key] - b[key]
            });
            this.setState({sortedColumn: key});
        } else {
            tableData.sort((a, b) => {
                return isNaN(+a[key]) ? b[key].localeCompare(a[key]) : b[key] - a[key]
            });
            this.setState({sortedColumn: ''});
        }
    };

    filterByColumn = (evt, key) => {
        const {filteredColumns} = this.state;
        const {tableData} = this.props;
        const inp = evt.target.value;
        const filterIndex = filteredColumns.map(obj => obj.key).indexOf(key);
        if (!inp) {
            filteredColumns.splice(filterIndex, 1)
        } else {
            ~filterIndex ? filteredColumns[filterIndex]={key,inp} : filteredColumns.push({key, inp});
        }
        let filteredData = [];
        if (filteredColumns.length) {
             filteredData = filteredColumns.reduce((newTableData, item) => {
                return newTableData.filter(row => row[item.key].match(item.inp))
            }, tableData);
        } else {
            filteredData = tableData;
        }

        this.setState({tableData: filteredData, filteredColumns});
    };

    componentWillReceiveProps(nextProps) {
        this.setState({tableData: nextProps.tableData});
    }

    render() {
        const {classes, tableHead, tableHeaderColor} = this.props;
        const {tableData} = this.state;
        return (
            <div className={classes.tableResponsive}>
                <Table className={classes.table}>
                    {tableHead !== undefined ? (
                        <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
                            <TableRow>
                                {tableHead.map((prop, key) => {
                                    return (
                                        <TableCell
                                            className={classes.tableCell + " " + classes.tableHeadCell}
                                            key={key}
                                        >
                                            <CustomInput
                                                labelText={prop}
                                                id="material"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    endAdornment: (
                                                        <InputAdornment position="end">
                                                            <Assignment/>
                                                        </InputAdornment>
                                                    ),
                                                    onChange: (evt) => this.filterByColumn(evt, key),
                                                    key: key
                                                }}

                                                // onClick={this.filterByColumn}
                                            />

                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        </TableHead>
                    ) : null}
                    <TableBody>
                        {tableData.map((prop, key) => {
                            return (
                                <TableRow key={key}>
                                    {prop.map((prop, key) => {
                                        return (
                                            <TableCell
                                                className={classes.tableCell}
                                                key={key}
                                                onClick={(evt) => this.sortColumn(evt, key)}
                                            >
                                                {prop}
                                            </TableCell>
                                        );
                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </div>
        );
    }
}

CustomTable.defaultProps = {
    tableHeaderColor: "gray"
};

CustomTable.propTypes = {
    classes: PropTypes.object.isRequired,
    tableHeaderColor: PropTypes.oneOf([
        "warning",
        "primary",
        "danger",
        "success",
        "info",
        "rose",
        "gray"
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

export default withStyles(tableStyle)(CustomTable);
