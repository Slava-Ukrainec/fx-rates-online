// import React, {Component} from 'react'
import serverRequests from '../requests/server-requests'

function getCityName(cityId, citiesObj) {
    return citiesObj[cityId]
}

function getCurrencyName(curId, curenciesObj) {
    return curenciesObj[curId]
}


function getCurrenciesArr () {
    serverRequests.getCurrenciesAPI().then(data => {
        const {currencies, cities, organizations} = data;
        const ratesMatrix = [];
        organizations.forEach(item => {
            for (let key in item.currencies) {
                const row = [];
                row.push(getCityName(item.cityId, cities));
                row.push(getCurrencyName(key, currencies));
                row.push(key);
                row.push(item.currencies[key].ask);
                row.push(item.currencies[key].bid);
                row.push(item.title);
                row.push(item.address);
                ratesMatrix.push(row);
            }
        });
        const date = data.date.split("").slice(0,10).join("");
        const time = data.date.split("").slice(11,16).join("");
        this.setState({
            fxrates:ratesMatrix,
            fxdate:{date,time}
        });
    });
}

export default getCurrenciesArr



